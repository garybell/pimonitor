# PiMonitor

Project for getting data from a Raspberry Pi, and storing it to allow the building of an external dashboard.

## Requirements
* Python 3.8.2 or higher (might work on lower versions, but this is untested)
* pip

## Installation
Clone the repository  
```bash
git clone https://gitlab.com/garybell/pimonitor.git
cd /path/to/pymonitor
pip3 install -r requirements.txt  # this might just be pip install -r requirements.txt depending on your system 
```

## Usage
This can run by calling 
`python3 pymonitor.py`
from the command line (or `python pymonitor.py` if there's only python 3 installed)
, or by using the `monitor.sh` script in the `bin/` folder.

It requires the file `config.yaml` to exist, which lists the database connection and drives to monitor.

## config.yaml
The config.yaml file will look like:
```yaml
database_dsn: localhost
drives:
  SSD: /dev/sda1
  Large HDD: /dev/sda2
  Mounted Drive: /media/mounted/drive
```

The `database_dsn` is the host for Redis (which is expected to be on the default port at present).  
The drives to monitor are listed below, and follow the following format: `drive name: mountpoint`.  Multiple drives can be added.