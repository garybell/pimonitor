from unittest import TestCase
from drives.drive import Drive


class Test(TestCase):
    def test_drive_empty_sets(self):
        drive = Drive()
        self.assertEqual('', drive.name)
        self.assertEqual('', drive.mount_point)
        self.assertEqual(0, drive.capacity_bytes)
        self.assertEqual(0, drive.used_bytes)

    def test_drive_values_set(self):
        drive = Drive('main', '/', 1000000000, 100000000)
        self.assertEqual('main', drive.name)
        self.assertEqual('/', drive.mount_point)
        self.assertEqual(1000000000, drive.capacity_bytes)
        self.assertEqual(100000000, drive.used_bytes)
        self.assertEqual(900000000, drive.get_free_space())
