import yaml
import os
from drives.drive import Drive
from database.redis_store import RedisStore


def load_config():
    with open(os.path.dirname(os.path.realpath(__file__)) + "/config.yaml", "r") as stream:
        config = yaml.safe_load(stream)
    return config


config = load_config()
print(config['drives'])
redis = RedisStore(config['database_dsn'])
# drives = json.loads(config['drives'])
for name, mountpoint in config['drives'].items():
    print("Processing %s at mountpoint %s" % (name, mountpoint))
    myDrive = Drive(name, mountpoint)
    redis.redis.hset('DRIVE:' + name, 'mountPoint', myDrive.mount_point)
    redis.redis.hset('DRIVE:' + name, 'capacityGB', (myDrive.capacity_bytes / 1024 ** 3))
    redis.redis.hset('DRIVE:' + name, 'usedGB', (myDrive.used_bytes / 1024 ** 3))
    redis.redis.hset('DRIVE:' + name, 'freeSpaceGB', (myDrive.get_free_space() / 1024 ** 3))
    print("Drive %s at mountpoint %s has capacity %f GB and %f GB free space" %
          (myDrive.name,
           myDrive.mount_point,
           (myDrive.capacity_bytes / (1024 ** 3)),
           (myDrive.get_free_space() / (1024 ** 3)),
           )
          )
