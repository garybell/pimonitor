#!/bin/bash
# call the pymonitor.py file with python3. We know it resides in the parent directory of this script
PARENT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd .. && pwd )"
python3 "$PARENT_DIRECTORY/pimonitor.py"