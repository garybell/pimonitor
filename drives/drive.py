import shutil


class Drive:
    def __init__(self, name='', mount_point=''):
        self.name = name
        self.mount_point = mount_point
        self.capacity_bytes = 0
        self.used_bytes = 0
        self.drive_info()

    def get_free_space(self):
        return self.capacity_bytes - self.used_bytes

    def drive_info(self):
        total, used, free = shutil.disk_usage(self.mount_point)
        self.capacity_bytes = total
        self.used_bytes = used
